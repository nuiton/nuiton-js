<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <!-- ************************************************************* -->
  <!-- *** POM Relationships *************************************** -->
  <!-- ************************************************************* -->

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>mavenpom4redmineAndCentral</artifactId>
    <version>7</version>
  </parent>

  <groupId>org.nuiton.js</groupId>
  <artifactId>nuiton-js</artifactId>
  <version>1.0.4-SNAPSHOT</version>

  <modules>
    <module>nuiton-js-wro</module>
    <module>nuiton-js-lib</module>
    <module>nuiton-js-test</module>
  </modules>

  <dependencyManagement>

    <dependencies>

      <!-- sl4j dependencies -->
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4jVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-simple</artifactId>
        <version>${slf4jVersion}</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>ro.isdc.wro4j</groupId>
        <artifactId>wro4j-core</artifactId>
        <version>${wroVersion}</version>
      </dependency>
      
      <dependency>
        <groupId>ro.isdc.wro4j</groupId>
        <artifactId>wro4j-maven-plugin</artifactId>
        <version>${wroVersion}</version>
        <scope>provided</scope>
      </dependency>
      
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>3.4</version>
      </dependency>
      
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
      </dependency>
      
      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>2.4</version>
      </dependency>

      <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>${servletApiVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>1.10.19</version>
        <scope>test</scope>
      </dependency>
    </dependencies>

  </dependencyManagement>

  <!-- ************************************************************* -->
  <!-- *** Project Information ************************************* -->
  <!-- ************************************************************* -->

  <name>Nuiton JS</name>
  <description>Common JS and CSS ressources for web applications</description>
  <inceptionYear>2012</inceptionYear>
  <url>http://doc.nuiton.org/nuiton-js/</url>

  <!-- Developpers, contributors... -->
  <developers>
    <developer>
      <id>tchemit</id>
      <name>Tony Chemit</name>
      <email>chemit at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>
    <developer>
      <id>jcouteau</id>
      <name>Jean Couteau</name>
      <email>couteau at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>
    <developer>
      <id>bpoussin</id>
      <name>Benjamin Poussin</name>
      <email>poussin at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>Développeur</role>
      </roles>
    </developer>
  </developers>

  <!-- ************************************************************* -->
  <!-- *** Build Settings ****************************************** -->
  <!-- ************************************************************* -->

  <packaging>pom</packaging>

  <properties>

    <wroVersion>1.7.9</wroVersion>
    <slf4jVersion>1.7.16</slf4jVersion>
    <servletApiVersion>2.5</servletApiVersion>
 
    <!-- redmine configuration -->
    <projectId>nuiton-js</projectId>

    <!-- site langue -->
    <siteLocales>fr</siteLocales>

    <!-- license to use  -->
    <license.licenseName>lgpl_v3</license.licenseName>

  </properties>

  <build>

    <pluginManagement>
      <plugins>

        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <configuration>
            <locales>en</locales>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.nuiton.jrst</groupId>
              <artifactId>doxia-module-jrst</artifactId>
              <version>${jrstPluginVersion}</version>
            </dependency>
          </dependencies>
        </plugin>

      </plugins>
    </pluginManagement>

    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
      </resource>
    </resources>
  </build>

  <reporting>
    <!--TC-20100413 : by default do nothing except documentation -->
    <excludeDefaults>true</excludeDefaults>
  </reporting>

  <!-- ************************************************************* -->
  <!-- *** Build Environment  ************************************** -->
  <!-- ************************************************************* -->

  <scm>
    <connection>scm:svn:https://svn.nuiton.org/nuiton-js/trunk</connection>
    <developerConnection>scm:svn:https://svn.nuiton.org/nuiton-js/trunk</developerConnection>
    <url>https://forge.nuiton.org/projects/nuiton-js/repository/show/trunk</url>
  </scm>

  <distributionManagement>
    <site>
      <id>doc.${platform}</id>
      <url>${our.site.repository}/${projectId}</url>
    </site>
  </distributionManagement>

  <profiles>

    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>

          <plugin>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>${projectInfoReportsPluginVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>project-team</report>
                  <report>mailing-list</report>
                  <report>cim</report>
                  <report>issue-tracking</report>
                  <report>license</report>
                  <report>scm</report>
                  <report>dependency-info</report>
                  <report>dependencies</report>   
                  <report>dependency-convergence</report>
                  <report>plugin-management</report>
                  <report>plugins</report>
                  <report>dependency-management</report>
                  <report>summary</report>
                </reports>
              </reportSet>
            </reportSets> 
          </plugin>

        </plugins>
      </reporting>
    </profile>
  </profiles>
</project>
