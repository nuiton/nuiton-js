/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.js.wro;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Model factory used to manage "non servlet context mode" and reduce, log error.
 * 
 * @author Eric Chatellier
 */
public class NuitonJsTestModelFactory extends NuitonJsXmlModelFactory {

    @Override
    protected InputStream getModelResourceAsStream() throws IOException {
        // just return empty file
        return new ByteArrayInputStream("<groups xmlns=\"http://www.isdc.ro/wro\" />".getBytes(Charset.forName("utf-8")));
    }

}
