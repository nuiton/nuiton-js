/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.js.wro;

import java.io.IOException;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.isdc.wro.cache.CacheKey;
import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.http.support.DelegatingServletOutputStream;
import ro.isdc.wro.model.WroModel;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.group.Group;
import ro.isdc.wro.model.group.processor.GroupsProcessor;
import ro.isdc.wro.model.group.processor.Injector;
import ro.isdc.wro.model.group.processor.InjectorBuilder;
import ro.isdc.wro.model.resource.ResourceType;
import ro.isdc.wro.util.io.NullOutputStream;

public class NuitonJsLibTest {

    private static final Logger LOG = LoggerFactory.getLogger(NuitonJsLibTest.class);

    /**
     * Load all available resources (wro-*.xml) file from classpath and try to parse model
     * to detect errors.
     * 
     * @throws IOException
     */
    @Test
    public void testModelLoad() throws IOException {
        // get configuration
        NuitonJsWroConfigurationFactory configFactory = new NuitonJsWroConfigurationFactory();
        WroConfiguration wroConfig = configFactory.create();

        // get test context
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);

        // ro.isdc.wro.WroRuntimeException: The processor: ro.isdc.wro.model.resource.processor.impl.css.CssUrlRewritingProcessor@34d18542 faile while processing uri: classpath:META-INF/resources/nuiton-js-jquery-ui/jquery-ui.css
        // at org.apache.commons.lang3.Validate.notNull(Validate.java:222)
        // at org.apache.commons.lang3.Validate.notNull(Validate.java:203)
        // at ro.isdc.wro.http.handler.ResourceProxyRequestHandler.createProxyPath(ResourceProxyRequestHandler.java:161)
        Mockito.when(request.getRequestURI()).thenReturn("/");

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        Mockito.when(response.getOutputStream()).thenReturn(new DelegatingServletOutputStream(new NullOutputStream()));
        FilterConfig filterConfig = Mockito.mock(FilterConfig.class);
        Context.set(Context.webContext(request, response, filterConfig), wroConfig);

        // init manager factory
        NuitonJsWroManagerFactory managerFactory = new NuitonJsTestManagerFactory();
        InjectorBuilder builder = new InjectorBuilder(managerFactory);
        Injector injector = builder.build();
        WroModelFactory modelFactory = managerFactory.newModelFactory();
        injector.inject(modelFactory);

        // parse model
        WroModel model = modelFactory.create();
        Assert.assertTrue("Can't find any valid wro groups !", model.getGroups().size() >= 1);
        LOG.info("Successfully parsed {} groups", model.getGroups().size());

        GroupsProcessor groupProcessor = new GroupsProcessor();
        injector.inject(groupProcessor);
        for (Group group : model.getGroups()) {
            String groupName = group.getName();
            LOG.info("Processing group {}", groupName);
            groupProcessor.process(new CacheKey(groupName, ResourceType.JS, false));
            groupProcessor.process(new CacheKey(groupName, ResourceType.CSS, false));
        }
    }
}
