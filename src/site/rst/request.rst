.. -
.. * #%L
.. * Nuiton JS
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====================
Request a new package
=====================

Using mailing list
==================

To request a new package, just ask for it using nuiton-js's mailing lists:
  * `nuiton-js-users@list.nuiton.org <http://list.nuiton.org/cgi-bin/mailman/listinfo/nuiton-js-users>`_

Please provide a description for these new package including license and download link.
