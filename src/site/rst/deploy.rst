.. -
.. * #%L
.. * Nuiton JS
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======================
Create new lib release
======================


Create new snapshot
===================

To create a new snapshot for a new or existing library, you have to create
a new module or copy an existing tagged library into sandbox library.

For example, to update jquery-ui library, perform following svn copy::

  svn copy http://svn.nuiton.org/svn/nuiton-js/tags/nuiton-js-jquery-ui-1.10.2-1 http://svn.nuiton.org/svn/nuiton-js/sandbox/nuiton-js-jquery-ui

Update code and project version containing "-SNAPSHOT" and deploy it as a snapshot::

  mvn deploy


Create new release
==================

To deploy a new release, copy an existing tags or sandbox snapshot into
a stable tag according to library version you want to deploy::

  svn move http://svn.nuiton.org/svn/nuiton-js/sandbox/nuiton-js-jquery-ui http://svn.nuiton.org/svn/nuiton-js/tags/nuiton-js-jquery-ui-1.10.3-1

Update version to remove all snapshot and deploy a released version::

  mvn clean deploy site-deploy -DperformRelease
  mvn -N -DdryRun=false -DperformRedminePreRelease
  mvn -N -DdryRun=false -DperformRedmineRelease
