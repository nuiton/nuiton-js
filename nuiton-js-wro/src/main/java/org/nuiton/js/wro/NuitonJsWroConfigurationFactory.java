package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.InputStream;
import java.util.Properties;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.factory.PropertyWroConfigurationFactory;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.util.ObjectFactory;

/**
 * Creation de notre propre configuration pour avoir des valeurs par defaut
 * dans la config:
 * <ul>
 * <li> managerFactoryClassName=org.nuiton.js.wro.NuitonJsWroManagerFactory</li>
 * <li> debug=true</li>
 * <li> preProcessors=fallbackCssDataUri,cssUrlRewriting,cssImport,semicolonAppender,cssMinJawr</li>
 * <li> postProcessors=cssVariables,jsMin</li>
 * <li> uriLocators=servletContext,uri,classpath</li>
 * </ul>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class NuitonJsWroConfigurationFactory implements ObjectFactory<WroConfiguration> {
    
    private static final Logger LOG = LoggerFactory.getLogger(NuitonJsWroConfigurationFactory.class);

    protected FilterConfig filterConfig;

    public NuitonJsWroConfigurationFactory() {
    }

    public NuitonJsWroConfigurationFactory(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public final WroConfiguration create() {
        Properties properties = initProperties();
        properties = loadContextProperties(properties);
        return new PropertyWroConfigurationFactory(properties).create();
    }

    /**
     * @return default path to configuration file relative to classpath / location.
     */
    protected String getUserConfigPath() {
        return "/nuiton-js.properties";
    }

    protected Properties getDefaultConfig() {
        Properties result = new Properties();
        result.setProperty("managerFactoryClassName", "org.nuiton.js.wro.NuitonJsWroManagerFactory");
        result.setProperty("debug", "true");
        // 20130103 ne pas utiliser cssMin car il ne support pas les "@media print" dans le CSS
        result.setProperty("preProcessors", "cssUrlRewriting,cssImport,semicolonAppender,cssMinJawr"); // rhinoLessCss fallbackCssDataUri,
        result.setProperty("postProcessors", "cssVariables,jsMin");
        result.setProperty("uriLocators","servletContext,uri,classpath");
        return result;
    }

    public Properties initProperties() {
        // Merge Properties file content with the default config content.
        Properties result = getDefaultConfig();

        InputStream propertyStream = null;
        String file = getUserConfigPath();
        try {
            LOG.debug("loading config resource from: {}", file);
            propertyStream = loadAsStream(file);
            if (propertyStream != null) {
                Properties props = new Properties();
                props.load(propertyStream);
                result.putAll(props);
            }
        } catch (final Exception e) {
            LOG.info("Cannot read properties file stream from default location: {}. Using default configuration.", file);
        } finally {
            IOUtils.closeQuietly(propertyStream);
        }

        return result;
    }

    /**
     * 
     * @param current
     * @return
     * @deprecated since 1.0.1 file moved to classpath, remove servlet context loading in 1.1+
     */
    @Deprecated
    protected Properties loadContextProperties(Properties current) {
        InputStream propertyStream = null;
        String file = "/WEB-INF/nuiton-js.properties";
        try {
            LOG.debug("loading config resource from: {}", file);
            propertyStream = getServletContext().getResourceAsStream(file);
            if (propertyStream != null) {
                LOG.warn("Loading configuration from deprecated {} location. Update configuration to use classpath '/nuiton-js.properties' instead",
                        file);
                Properties props = new Properties();
                props.load(propertyStream);
                current.putAll(props);
            }
        } catch (final Exception e) {
            LOG.debug("Cannot read properties file stream from servlet context location: {}.", file);
        } finally {
            IOUtils.closeQuietly(propertyStream);
        }
        return current;
    }

    protected InputStream loadAsStream(String file) throws Exception {
        InputStream result = NuitonJsWroConfigurationFactory.class.getResourceAsStream(file);
        return result;
    }

    /**
     * @deprecated since 1.0.1 file moved to classpath, remove servlet context loading in 1.1+
     */
    protected ServletContext getServletContext() {
        ServletContext result;
        if (filterConfig != null) {
            result = filterConfig.getServletContext();
        } else {
            result = Context.get().getServletContext();
        }
        return result;
    }

}
