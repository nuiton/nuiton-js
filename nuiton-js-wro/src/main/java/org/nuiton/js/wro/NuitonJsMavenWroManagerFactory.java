package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import ro.isdc.wro.manager.factory.standalone.ConfigurableStandaloneContextAwareManagerFactory;
import ro.isdc.wro.manager.factory.standalone.DefaultStandaloneContextAwareManagerFactory;
import ro.isdc.wro.manager.factory.standalone.StandaloneContext;
import ro.isdc.wro.maven.plugin.support.ExtraConfigFileAware;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;
import ro.isdc.wro.model.resource.processor.factory.ConfigurableProcessorsFactory;
import ro.isdc.wro.model.resource.processor.factory.ProcessorsFactory;
import ro.isdc.wro.model.resource.support.hash.ConfigurableHashStrategy;
import ro.isdc.wro.model.resource.support.hash.HashStrategy;
import ro.isdc.wro.model.resource.support.naming.ConfigurableNamingStrategy;
import ro.isdc.wro.model.resource.support.naming.NamingStrategy;

/**
 * ManagerFactory a utiliser pour le plugin maven.
 *
 * Cette classe pourrait heriter de ConfigurableWroManagerFactory, mais
 * a cause de methode final :( dans
 * {@link ConfigurableStandaloneContextAwareManagerFactory#newProcessorsFactory},
 * on doit directement heriter de DefaultStandaloneContextAwareManagerFactory
 *
 * Mais comme au final vu qu'on surchargeait beaucoup de chose, il n'y a que
 * deux methodes en plus {@link #newNamingStrategy} et {@link #newHashStrategy}
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class NuitonJsMavenWroManagerFactory extends DefaultStandaloneContextAwareManagerFactory
    implements ExtraConfigFileAware {
    protected StandaloneContext standaloneContext;
    protected File configProperties;

    /**
     * Surcharge pour ajouter les preprocessors {@link ForceCssDataUriPreProcessor}
     * @return
     */
    @Override
    protected ProcessorsFactory newProcessorsFactory() {
        return new ConfigurableProcessorsFactory(){
            @Override
            protected Map<String, ResourcePreProcessor> newPreProcessorsMap() {
                Map<String, ResourcePreProcessor> result = super.newPreProcessorsMap();
                result.put(ForceCssDataUriPreProcessor.ALIAS, new ForceCssDataUriPreProcessor());
                return result;
            }

        }.setProperties(createProperties());
    }

    /**
     * Surcharge pour garde le {@link StandaloneContext}
     * @param standaloneContext
     */
    @Override
    public void initialize(final StandaloneContext standaloneContext) {
        super.initialize(standaloneContext);
        this.standaloneContext = standaloneContext;
    }

    /**
     * Surcharge pour utilise le {@link NuitonJsXmlModelFactory}
     * @return
     */
    @Override
    protected WroModelFactory newModelFactory() {
        return new NuitonJsXmlModelFactory() {
            @Override
            protected InputStream getModelResourceAsStream() throws IOException {
                InputStream stream = new BufferedInputStream(
                        new FileInputStream(standaloneContext.getWroFile()));
                return stream;
            }
        };
    }

    /**
     * Utiliser pour initialiser les fichiers de config avec les choix fait
     * par Nuiton-js et permettre la surcharge via le fichier definit dans la
     * configuration maven
     * @return
     */
    protected Properties createProperties() {
        Properties result = new NuitonJsWroConfigurationFactory(){
            /**
             * On utilise le fichier indique dans la configuration maven
             * au lieu du fichier par defaut
             */
            @Override
            protected String getUserConfigPath() {
                return configProperties.getAbsolutePath();
            }
            /**
             * Surcharge pour recherche les fichiers sur le filesystem ou lieu
             * d'utiliser le context servlet dans lequel on est pas
             */
            @Override
            protected InputStream loadAsStream(String file) throws Exception {
                return new BufferedInputStream(new FileInputStream(file));
            }
        }.initProperties();
        return result;
    }

    /**
     * Recupere le fichier indique dans la configuration maven
     * @param extraProperties
     */
    @Override
    public void setExtraConfigFile(final File extraProperties) {
        this.configProperties = extraProperties;
    }

  /**
   * {@inheritDoc}
   */
  @Override
  protected NamingStrategy newNamingStrategy() {
    return new ConfigurableNamingStrategy() {
      @Override
      protected Properties newProperties() {
        return createProperties();
      }
    };
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected HashStrategy newHashStrategy() {
    return new ConfigurableHashStrategy() {
      @Override
      protected Properties newProperties() {
        return createProperties();
      }
    };
  }


}
