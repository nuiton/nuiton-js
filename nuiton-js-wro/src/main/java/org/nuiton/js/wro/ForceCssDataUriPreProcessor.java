package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import ro.isdc.wro.model.resource.processor.impl.css.CssDataUriPreProcessor;

/**
 * Processor qui force l'utilisation des dataUri meme pour les grosses ressource
 * Ceci est utile lorsque l'on souhaite deployer les CSS sans avoir le filtre WRO
 * (par exemple suite a la creation via le plugin maven)
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ForceCssDataUriPreProcessor extends CssDataUriPreProcessor {
    
    public static final String ALIAS = "forceCssDataUri";

    @Override
    protected boolean isReplaceAccepted(String dataUri) {
        return true;
    }


}
