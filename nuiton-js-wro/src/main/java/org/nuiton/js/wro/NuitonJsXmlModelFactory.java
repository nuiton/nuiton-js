package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import ro.isdc.wro.WroRuntimeException;
import ro.isdc.wro.model.WroModel;
import ro.isdc.wro.model.WroModelInspector;
import ro.isdc.wro.model.group.Group;

/**
 * On etend la classe pour forcer le chargement de toutes les librairies de
 * nuiton-js
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class NuitonJsXmlModelFactory extends ImportWildcardXmlModelFactory {

    private static final Logger LOG = LoggerFactory.getLogger(NuitonJsXmlModelFactory.class);

    protected String getNuitonJsPatternXmlFile() {
        return "classpath:META-INF/nuiton-js/wro-*.xml";
    }

    @Override
    public synchronized WroModel create() {
        // creation du model
        model = new WroModel();

        // on ajoute toutes les les fichiers XML de nuiton-js
        String pattern = getNuitonJsPatternXmlFile();
        List<String> imports = findAll(pattern);
        for (String importName : imports) {

            if (processedImports.contains(importName)) {
                final String message = "Import already done: " + importName;
                LOG.warn(message);
//                throw new RecursiveGroupDefinitionException(message);
            } else {
                processedImports.add(importName);
                merge(model, createImportedModel(importName));
            }
        }

        // on continue avec le fichier par defaut /WEB-INF/wro.xml
        try {
            // on ne reutilise pas create du super car la methode commence
            // par un new model :(. Or on veut conserve les imports qui viennent
            // d'etre fait, et ne pas oblige l'utilisateur a les refaire dans
            // sont modele
            try {
                Document document = createDocument();
                processGroups(document);
                processImports(document);
                parseGroups();
            } finally {
                // clear the processed imports even when the model creation fails.
                processedImports.clear();
            }
        } catch (WroRuntimeException eee) {
            // si pas de fichier utilisateur on utilisera que les lib de nuiton-js
            // donc on ne fait qu'afficher un warning
            LOG.warn("No user wro.xml file found, use only nuiton-js declaration", eee);
        }
        
        return model;
    }

  /**
   * Merge master model with another model. This is useful for supporting model imports.
   *
   * @param master master modele where we put imported model
   * @param importedModel model to import.
   */
  protected void merge(WroModel master, WroModel importedModel) {
    Validate.notNull(importedModel, "imported model cannot be null!");
    LOG.debug("merging importedModel: {}", importedModel);
    for (final String groupName : new WroModelInspector(importedModel).getGroupNames()) {
      if (!new WroModelInspector(master).getGroupNames().contains(groupName)) {
          // si les deux modeles contiennent le meme groupe, on garde celui du master
          final Group importedGroup = new WroModelInspector(importedModel).getGroupByName(groupName);
          master.addGroup(importedGroup);
      }
    }
  }

}
