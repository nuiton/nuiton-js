package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Properties;
import ro.isdc.wro.manager.factory.ConfigurableWroManagerFactory;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

/**
 * Surcharge pour force la recherche du fichier /WEB-INF/nuiton-js.properties
 * a la place de /WEB-INF/wro.properties. Et chargement de certaine  valeur par
 * defaut dans de la config
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class NuitonJsWroManagerFactory extends ConfigurableWroManagerFactory {

    @Override
    protected WroModelFactory newModelFactory() {
        return new NuitonJsXmlModelFactory();
    }

    @Override
    protected Properties newConfigProperties() {
        Properties result = new NuitonJsWroConfigurationFactory().initProperties();
        return result;
    }

    @Override
    protected void contributePreProcessors(Map<String, ResourcePreProcessor> map) {
        super.contributePreProcessors(map);
        map.put(ForceCssDataUriPreProcessor.ALIAS, new ForceCssDataUriPreProcessor());
    }

}
