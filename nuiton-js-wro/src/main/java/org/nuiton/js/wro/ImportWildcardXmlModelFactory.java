package org.nuiton.js.wro;

/*
 * #%L
 * Nuiton JS :: WRO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ro.isdc.wro.model.group.RecursiveGroupDefinitionException;
import ro.isdc.wro.model.resource.locator.ClasspathUriLocator;
import ro.isdc.wro.model.resource.locator.UriLocator;
import ro.isdc.wro.model.resource.locator.wildcard.DefaultWildcardStreamLocator;
import ro.isdc.wro.model.resource.locator.wildcard.WildcardExpanderHandlerAware;
import ro.isdc.wro.model.resource.locator.wildcard.WildcardStreamLocator;
import ro.isdc.wro.model.resource.locator.wildcard.WildcardUriLocatorSupport;
import ro.isdc.wro.model.transformer.WildcardExpanderModelTransformer;
import ro.isdc.wro.util.Function;

/**
 * Surcharge de ProtectedXmlModelFactory pour permettre d'avoir des * et **
 * dans les imports de la meme facon que dans les ressources.
 *
 * Beaucoup de ce code provient du code initial qui n'etait pas a mon sens tres
 * propre/comprehensible. Un bon refactore est necessaire (a mois que la fonctionnalite
 * soit implanter directement dans le projet initial, mais vu comment c'est code
 * ce ne sera pas facile, il y a d'ailleurs deja un ticket a cause de la facon
 * de charger les fichiers dans le classpath et les deux sont lie
 * (Issue 593: cannot use resource wildcards for the same classpath in multiple JARs))
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportWildcardXmlModelFactory extends ProtectedXmlModelFactory {

    /**
     * Logger for this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImportWildcardXmlModelFactory.class);

    @Override
    protected void processImports(Document document) {
        final NodeList importsList = document.getElementsByTagName(TAG_IMPORT);
        LOG.debug("number of imports: {}", importsList.getLength());
        for (int i = 0; i < importsList.getLength(); i++) {
            final Element element = (Element) importsList.item(i);
            final String name = element.getTextContent();
            LOG.debug("processing import: {}", name);
            Validate.notNull(locatorFactory, "The Locator cannot be null!");

            // recherche de tous les imports a faire s'il y a des *
            List<String> imports = findAll(name);
            for (String importName : imports) {

                if (processedImports.contains(importName)) {
                    final String message = "Recursive import detected: " + importName;
                    LOG.error(message);
                    throw new RecursiveGroupDefinitionException(message);
                }

                processedImports.add(importName);
                model.merge(createImportedModel(importName));
            }
        }
    }

    /**
     * Recherche tous les fichiers correspondant a l'uri
     * @param uri
     * @return
     */
    protected List<String> findAll(String uri) {
        List<String> result = new ArrayList<String>();

        final UriLocator uriLocator = locatorFactory.getInstance(uri);

        if (uriLocator instanceof WildcardUriLocatorSupport) {
            final WildcardStreamLocator wildcardStreamLocator = ((WildcardUriLocatorSupport) uriLocator).getWildcardStreamLocator();

            if (wildcardStreamLocator.hasWildcard(uri)
                    && wildcardStreamLocator instanceof WildcardExpanderHandlerAware) {
                LOG.debug("Expanding import: {}", uri);

                if (StringUtils.startsWith(uri, ClasspathUriLocator.PREFIX)) {
                    // on traite les imports classpath: avec * differement pour supporte les multiples jar
                    String fullPath = StringUtils.substringAfter(uri, ClasspathUriLocator.PREFIX);
                    // on supprime la fin qui contient les * ou ** ou ?
                    String beginPath = FilenameUtils.getFullPathNoEndSeparator(fullPath);
                    try {
                        Enumeration<URL> jars = Thread.currentThread().getContextClassLoader().getResources(beginPath);
                        while (jars.hasMoreElements()) {
                            URL url = jars.nextElement();
                            List<String> files = listAcceptableFile(fullPath, url);
                            result.addAll(files);
                        }
                    } catch (final IOException e) {
                        e.printStackTrace(); // DEBUG
                        // log only
                        LOG.warn("[FAIL] problem while trying to expand wildcard for the following resource uri: {}",
                                uri);
                    }
                } else {
                    final WildcardExpanderHandlerAware expandedHandler = (WildcardExpanderHandlerAware) wildcardStreamLocator;

                    final String baseNameFolder = computeBaseNameFolder(uri, uriLocator, expandedHandler);
                    LOG.debug("baseNameFolder: {}", baseNameFolder);

                    final ThreadLocal<List<String>> resultLocal = new ThreadLocal<List<String>>();
                    expandedHandler.setWildcardExpanderHandler(
                            createExpanderHandler(uri, baseNameFolder, resultLocal));
                    try {
                        // trigger the wildcard replacement
                        uriLocator.locate(uri);
                    } catch (final IOException e) {
                        e.printStackTrace(); // DEBUG
                        // log only
                        LOG.warn("[FAIL] problem while trying to expand wildcard for the following resource uri: {}",
                                uri);
                    } finally {
                        // remove the handler, it is not needed anymore
                        expandedHandler.setWildcardExpanderHandler(null);
                    }
                    result = resultLocal.get();
                }
            } else {
                // pas de wildcard, on a qu'une uri
                result.add(uri);
            }
        } else {
            // pas de wildcard, on a qu'une uri
            result.add(uri);
        }
        return result;
    }

    /**
     *
     * @param pattern le pattern recherche de la forme: a/b/c/**.xml
     * @param url l'url du jar dans lequel il faut faire la recherche
     * @return la liste des fichiers du jar qui correspond
     * @throws IOException
     */
    protected List<String> listAcceptableFile(String pattern, URL url) throws IOException {
        // le path du pattern (c-a-d: a/b/c/)
        String classPath = FilenameUtils.getPath(pattern);

        // fix strange charaters on windows
        String filePath = URLDecoder.decode(url.getPath(), "UTF-8");

        // recuperation du path du jar sans la partie query
        // use toURI to fix windows bugs with space path
        String jarPath = StringUtils.substringBefore(filePath, "!");
        if (jarPath.startsWith("file:")) {
            jarPath = StringUtils.substringAfter(jarPath, "file:");
        }
        File jarFile = new File(jarPath);

        List<String> allFiles = new ArrayList<String>();

        // dans le classpath on peut avoir des repertoires ou des fichiers
        if (jarFile.isDirectory()) {
            String filePattern = FilenameUtils.getName(pattern);
            Collection<File> files = FileUtils.listFiles(jarFile, 
                    new WildcardFileFilter(filePattern),
                    TrueFileFilter.INSTANCE);
            for (File f : files) {
                allFiles.add(f.toURI().toURL().toString());
            }
        } else if (FilenameUtils.isExtension(jarPath, new String[]{"jar", "zip", "war"})) {

            String prefix = "jar:" + StringUtils.substringBefore(filePath, "!") + "!/";

            JarFile file = null;
            try {
                file = new JarFile(jarPath);
    
                List<JarEntry> jarEntryList = Collections.list(file.entries());
                for (final JarEntry entry : jarEntryList) {
                    final String entryName = entry.getName();
                    //ignore the parent folder itself and accept only child resources
                    final boolean isSupportedEntry = entryName.startsWith(classPath)
                            && !entryName.equals(classPath)
                            && FilenameUtils.wildcardMatch(entryName, pattern);
                    if (isSupportedEntry) {
                        String u = prefix + entryName;
                        allFiles.add(u);
                        LOG.debug("\tfound jar entry: {}", entryName);
                    }
                }
            } finally {
                if (file != null) {
                    file.close();
                }
            }
        } else {
            LOG.warn("Not supported import type {}", jarPath);
        }
        return allFiles;
    }
    //
    // FROM WildcardExpanderModelTransformer
    //

    /**
     * Computes the file name of the folder where the resource is located. The implementation uses a trick by invoking the
     * {@link WildcardExpanderHandlerAware} to get the baseName.
     */
    private String computeBaseNameFolder(final String uri, final UriLocator uriLocator,
            final WildcardExpanderHandlerAware expandedHandler) {
        // Find the baseName
        // add a recursive wildcard to trigger the wildcard detection. The simple wildcard ('*') is not enough because it
        // won't work for folders containing only directories with no files.
        LOG.debug("computeBaseNameFolder for import {}", uri);
        final String resourcePath = FilenameUtils.getFullPath(uri)
                + DefaultWildcardStreamLocator.RECURSIVE_WILDCARD;
        LOG.debug("resourcePath: {}", resourcePath);
        // use thread local because we need to assign a File inside an anonymous class and it fits perfectly
        final ThreadLocal<String> baseNameFolderHolder = new ThreadLocal<String>();
        expandedHandler.setWildcardExpanderHandler(createBaseNameComputerFunction(baseNameFolderHolder));

        try {
            uriLocator.locate(resourcePath);
        } catch (final Exception e) {
            LOG.debug("[FAIL] Exception caught during wildcard expanding for resource: {}\n with exception message {}",
                    resourcePath, e.getMessage());
        }
        if (baseNameFolderHolder.get() == null) {
            LOG.debug("[FAIL] Cannot compute baseName folder for resource: {}", uri);
        }
        return baseNameFolderHolder.get();
    }

    private Function<Collection<File>, Void> createBaseNameComputerFunction(final ThreadLocal<String> baseNameFolderHolder) {
        return new Function<Collection<File>, Void>() {
            public Void apply(final Collection<File> input)
                    throws Exception {
                LOG.debug("\texpanded Files: {}", input);
                for (final File file : input) {
                    LOG.debug("\tsetting baseNameFolder: {}", file.getParent());
                    baseNameFolderHolder.set(file.getParent());
                    // no need to continue
                    break;
                }
                // use this to skip wildcard stream detection, we are only interested in the baseName
                throw new WildcardExpanderModelTransformer.NoMoreAttemptsIOException("BaseNameFolder computed successfully, skip further wildcard processing..");
            }
        };
    }
    
    /**
     * create the handler which expand the import containing wildcard.
     */
    public Function<Collection<File>, Void> createExpanderHandler(final String uri,
            final String baseNameFolder, final ThreadLocal<List<String>> resultLocal) {
        LOG.debug("createExpanderHandler using baseNameFolder: {}\n for import {}", baseNameFolder, uri);
        final Function<Collection<File>, Void> handler = new Function<Collection<File>, Void>() {
            public Void apply(final Collection<File> files) {
                if (baseNameFolder == null) {
                    LOG.warn("The import {} is probably invalid, removing it from the import.", uri);
                    resultLocal.set(new ArrayList<String>());
                } else {
                    final List<String> expandedImports = new ArrayList<String>();
                    LOG.debug("baseNameFolder: {}", baseNameFolder);
                    for (final File file : files) {
                        final String importPath = getFullPathNoEndSeparator(uri);
                        LOG.debug("\tresourcePath: {}", importPath);
                        LOG.debug("\tfile path: {}", file.getPath());
                        final String computedImportUri = importPath
                                + StringUtils.removeStart(file.getPath(), baseNameFolder).replace('\\', '/');
                        
                        LOG.debug("\texpanded resource: {}", computedImportUri);
                        expandedImports.add(computedImportUri);
                    }
                    LOG.debug("\treplace resource {}", uri);
                    resultLocal.set(expandedImports);
                }
                return null;
            }
            
            /**
             * This method fixes the problem when a resource in a group uses deep wildcard and starts at the root.
             * <p/>
             * Find more details <a href="https://github.com/alexo/wro4j/pull/44">here</a>.
             */
            private String getFullPathNoEndSeparator(final String uri) {
                String result = FilenameUtils.getFullPathNoEndSeparator(uri);
                if (result != null && 1 == result.length() && 0 == FilenameUtils.indexOfLastSeparator(result))
                    return "";
                
                return result;
            }
        };
        return handler;
    }

}
