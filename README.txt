Objectif :
----------

Permettre de packager des ressources (CSS, JS) pour les utiliser en tant que
dépendance Maven.

Décomposition en modules :
--------------------------

Chaque ressource a son propre module qui peut dépendre d'un autre (ex:
jqueryui -> jquery)